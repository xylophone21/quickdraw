import os
import torchvision
import glob
import sys
import bisect
import numpy as np
import time
from PIL import Image
from tqdm.autonotebook import tqdm

s_images_per_file = 10000
def _make_datasets(root,classes,class_to_idx,start,max_count,debug):
    if start < 0 or max_count <= 0: 
        raise (RuntimeError("range error"))

    instances = []
    all_count = 0
    image_count = [0]
    processor = tqdm(classes,leave=False)
    for idx,clazz in enumerate(processor):
        loaded_count = 0
        last_idx = start + max_count - 1
        first_file = int(start / s_images_per_file)
        last_file = int(last_idx / s_images_per_file)

        if debug:
            print("find class",clazz,start,last_idx,first_file,last_file)

        for idx in range(first_file,last_file+1):
            file = os.path.join(root,clazz + "/" + str(idx) + ".npy")
            if os.path.exists(file):
                imgs_load = np.load(file)
                clazz_count = imgs_load.shape[0]
                org_clazz_count = clazz_count

                if idx == first_file and start % s_images_per_file != 0:
                    inner_start = start % s_images_per_file
                    clazz_count -= inner_start

                    if clazz_count <= 0:
                        print("   error",file,imgs_load.shape,inner_start)
                        break

                    imgs_load = imgs_load[inner_start:]
                elif idx == last_file:
                    clazz_count = max_count - loaded_count

                    if clazz_count > imgs_load.shape[0]:
                        clazz_count = imgs_load.shape[0]
                        
                    imgs_load = imgs_load[:clazz_count]

                loaded_count += clazz_count

                if debug:
                    print("   open",file,org_clazz_count,clazz_count,imgs_load.shape,len(instances))
                    
                item = imgs_load,class_to_idx[clazz]
                instances.append(item)
                all_count += clazz_count
                image_count.append(all_count)
            else:
                break

    return instances,all_count,image_count

def _find_classes(root, clazz_start,clazz_count):
    classes = []
    folders = npzs_folders = glob.glob(root + "/*",recursive=False)
    folders.sort()
    
    if clazz_count <= 0:
        clazz_count = len(folders)
    for idx,folder in enumerate(folders):
        _,clazz = os.path.split(folder)
        if idx >= clazz_start and idx < clazz_start + clazz_count:
            classes.append(clazz)

    if len(classes) > clazz_count :
        raise (RuntimeError("clazz_list error"))

    classes.sort()
    class_to_idx = {classes[i]: i for i in range(len(classes))}
    return classes, class_to_idx


class ImageFromBitNpy(torchvision.datasets.vision.VisionDataset):
    """A generic data loader where the samples are arranged in a pandas.DataFrame.
    """

    def __init__(self, root, clazz_start=0,clazz_count=-1, start=0,max_count=sys.maxsize,debug=False,
                 output_img=True,
                 transform=None,
                 target_transform=None):
        super(ImageFromBitNpy, self).__init__(None, transform=transform,
                                          target_transform=target_transform)

        classes, class_to_idx = _find_classes(root,clazz_start,clazz_count)
        if len(classes) == 0:
            raise (RuntimeError("Found 0 classes in root"))

        samples,all_count,image_count = _make_datasets(root,classes,class_to_idx,start,max_count,debug)
        if len(samples) == 0:
            raise (RuntimeError("Found 0 files in df"))

        self.root = root
        self.classes = classes
        self.class_to_idx = class_to_idx
        self.start = start
        self.samples = samples
        self.count = all_count
        self.image_count = image_count
        self.debug = debug
        self.output_img = output_img

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (sample, target) where target is class_index of the target class.
        """
        #since = time.time()
        ins_idx = bisect.bisect_right(self.image_count,index) - 1
        ins,class_idx = self.samples[ins_idx]
        before_count = self.image_count[ins_idx]
        inner_index = index - before_count
        img_un = np.unpackbits(ins[inner_index])

        if self.output_img:
            img_un = img_un.reshape(64,64,1).repeat(3,axis=2) * 255
            img_un = Image.fromarray(img_un)

        if self.debug:
            print(f"getitem,{index}, ins_idx={ins_idx}, inner_index={inner_index}, before_count={before_count}, class_idx={class_idx}, class={self.classes[class_idx]}")

        if self.transform is not None:
            img_un = self.transform(img_un)
        if self.target_transform is not None:
            class_idx = self.target_transform(class_idx)

        #print("all",time.time() - since)
        return img_un,class_idx

    def __len__(self):
        return self.count
