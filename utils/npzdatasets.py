import os
import torchvision
import glob
import sys
import bisect
from tqdm.autonotebook import tqdm
import numpy as np
import time

s_max_count_per_class = 1000000

#todo: lots of range check needs
def _find_classes_length(root,classes,start,max_count,debug):
    if start < 0 or max_count <= 0: 
        raise (RuntimeError("range error"))

    all_count = 0
    all_list = []
    processor = tqdm(classes,leave=False)
    for idx,clazz in enumerate(processor):
        max_file = os.path.join(root,clazz + "/" + clazz + "-" + str(start + max_count - 1) + ".npz")
        if os.path.exists(max_file):
            clazz_count = max_count
        else:
            npzs = glob.glob(root + "/" + clazz + "/*.npz",recursive=False)
            count = len(npzs)
            clazz_count = count - start

            if clazz_count < 0:
                clazz_count = 0

        if debug:
            print(clazz,clazz_count)
        if clazz_count >= s_max_count_per_class:
            raise (RuntimeError("class %s count too big-%d" % (clazz,clazz_count)))
        all_count += clazz_count
        l = np.arange(s_max_count_per_class*idx + start,s_max_count_per_class*idx + start + clazz_count)
        all_list.append(l)

    all_list_np = np.concatenate(all_list)

    if debug:
        print("all_list_np",all_list_np)

    return all_count,all_list_np


def _find_classes(root, clazz_list):
    classes = []
    folders = npzs_folders = glob.glob(root + "/*",recursive=False)
    for folder in folders:
        _,clazz = os.path.split(folder)
        if clazz_list == [] or clazz in clazz_list:
            classes.append(clazz)

    if clazz_list != [] and len(clazz_list) > len(classes):
        raise (RuntimeError("clazz_list error"))

    classes.sort()
    class_to_idx = {classes[i]: i for i in range(len(classes))}
    return classes, class_to_idx


class ImageFromFolder(torchvision.datasets.vision.VisionDataset):
    """A generic data loader where the samples are arranged in a pandas.DataFrame.
    """

    def __init__(self, root, clazz_list=[], start=0,max_count=sys.maxsize,debug=False,
                 transform=None,
                 target_transform=None):
        super(ImageFromFolder, self).__init__(None, transform=transform,
                                          target_transform=target_transform)

        classes, class_to_idx = _find_classes(root, clazz_list)
        if len(classes) == 0:
            raise (RuntimeError("Found 0 classes in root"))

        count,all_list_np = _find_classes_length(root,classes,start,max_count,debug)
        if count <= 0:
            raise (RuntimeError("Found 0 files in root"))

        self.root = root
        self.classes = classes
        self.class_to_idx = class_to_idx
        self.start = start
        self.count = count
        self.all_list_np = all_list_np
        self.debug = debug

    def shuffle(self):
        np.random.shuffle(self.all_list_np)

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (sample, target) where target is class_index of the target class.
        """
        #since1 = time.time()
        pos = self.all_list_np[index]
        clazz = self.classes[int(pos/s_max_count_per_class)]
        class_idx = self.class_to_idx[clazz]
        inner_index = pos%s_max_count_per_class
        filename = "%s/%s-%d.npz" % (clazz,clazz,inner_index)
        if self.debug:
            print(index,filename)
        npz_file = os.path.join(self.root,filename)

        #since = time.time()
        sample = np.load(npz_file)['img']
        #print("load",time.time() - since)

        if self.transform is not None:
            sample = self.transform(sample)
        if self.target_transform is not None:
            class_idx = self.target_transform(class_idx)

        #print("all",time.time() - since)
        return sample,class_idx

    def __len__(self):
        return self.count
