import numpy as np
import cairocffi as cairo

def vector_to_image(vector_images, line_diameter=5, bg_color=(0,0,0), fg_color=(1,1,1),new_side = 256):

    original_side = 256

    scale = new_side/original_side

    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, new_side, new_side)
    ctx = cairo.Context(surface)
    ctx.set_antialias(cairo.ANTIALIAS_BEST)
    ctx.set_line_cap(cairo.LINE_CAP_ROUND)
    ctx.set_line_join(cairo.LINE_JOIN_ROUND)
    ctx.set_line_width(line_diameter)

    ctx.set_source_rgb(*bg_color)
    ctx.paint()

    for stroke in vector_images:
        for step in range(0, len(stroke[0])-1):
            ctx.set_source_rgb(*fg_color)
            x = stroke[0][step] * scale
            y = stroke[1][step] * scale
            ctx.move_to(x, y)
            x1 = stroke[0][step+1] * scale
            y1 = stroke[1][step+1] * scale
            ctx.line_to(x1,y1)
    ctx.stroke()

    data = surface.get_data()
    newscratch = np.copy(np.asarray(data)[::4])
    image = newscratch.reshape(new_side, new_side)

    return image
